<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php
    open_connection_DB();
    // Vérifier si le deck existe déjà ou s'il doit être initialisé
    if (!isset($_SESSION['deck'])) {
            // Initialisation du deck
        $_SESSION['deck'] = array(
            'nbCartes' => $_SESSION['nbCP'], // Nombre initial de cartes dans le deck
            'cartes' => array() // Tableau vide pour stocker les cartes
        );

        // Générer les cartes pour chaque couleur
        $nbCartesVertes = $_SESSION['nbCV'];
        $nbCartesOranges = $_SESSION['nbCO'];
        $nbCartesNoires = $_SESSION['nbCN'];

        // Points et niveaux par couleur
        $pointsParCouleur = array('V' => 2, 'O' => 4, 'N' => 6);
        $imageFolder = 'img/ressources_graphiques/';

        // Générer les cartes vertes
        for ($i = 1; $i <= $nbCartesVertes; $i++) {
            $nouvelleCarte = array(
                'idC' => 'V' . str_pad($i, 3, '0', STR_PAD_LEFT), // ID unique pour les cartes vertes
                'niveau' => 'V',
                'points' => $pointsParCouleur['V'], // Points pour les cartes vertes
                'img' => $imageFolder . 'V' . str_pad($i, 3, '0', STR_PAD_LEFT) . '.png'
            );

            // Ajouter la nouvelle carte au deck
            $_SESSION['deck']['cartes'][] = $nouvelleCarte;
        }


    // Générer les cartes oranges
    for ($i = 1; $i <= $nbCartesOranges; $i++) {
        $nouvelleCarte = array(
            'idC' => 'O' . str_pad($i, 3, '0', STR_PAD_LEFT), // ID unique pour les cartes oranges
            'niveau' => 'O',
            'points' => $pointsParCouleur['O'], // Points pour les cartes oranges
            'img' => $imageFolder . 'O' . str_pad($i, 3, '0', STR_PAD_LEFT) . '.png'
        );
        $_SESSION['deck']['cartes'][] = $nouvelleCarte;
    }

    // Générer les cartes noires
    for ($i = 1; $i <= $nbCartesNoires; $i++) {
        $nouvelleCarte = array(
            'idC' => 'N' . str_pad($i, 3, '0', STR_PAD_LEFT), // ID unique pour les cartes noires
            'niveau' => 'N',
            'points' => $pointsParCouleur['N'], // Points pour les cartes noires
            'img' => $imageFolder . 'N' . str_pad($i, 3, '0', STR_PAD_LEFT) . '.png'
        );
        $_SESSION['deck']['cartes'][] = $nouvelleCarte;
    }

    // Mélanger les cartes dans le deck
    shuffle($_SESSION['deck']['cartes']);



        // Mélanger les cartes du deck
        shuffle($_SESSION['deck']['cartes']);

        }


    //$deck = tirageCartes($_SESSION['nbCV'], $_SESSION['nbCO'], $_SESSION['nbCN']);

if (!isset($_SESSION['position'])) {
    // Initialiser les positions des pions
    $_SESSION['position'] = initialiser_positions($_SESSION['indice_joueur']);
        
    $tabScore = array(); // Initialiser le tableau en dehors de la boucle
    foreach ($_SESSION['indice_joueur'] as $idJ) {
        $donneeScore = executer_une_requete("SELECT score FROM `JOUE` WHERE idJ = '" . $idJ . "'");
        // Vérifier si la requête a réussi et si des données ont été retournées
        if ($donneeScore !== false && !empty($donneeScore)) {
            // Récupérer le score du joueur à partir du résultat de la requête
            // Dans votre cas, $donneeScore est déjà un tableau associatif contenant les données de la requête
            // Vous pouvez accéder au score directement en utilisant la clé 'score'
            $tabScore[$idJ] = $donneeScore[0]['score'];
        } else {
            // Si la requête échoue ou si aucune donnée n'est retournée, définir le score à zéro
            $tabScore[$idJ] = 0;
            $joueAct = 0;
            if ($idJ == 1) {$joueAct = 1;}  
            //$a = executer_une_requete("INSERT INTO HISTORIQUE (idJ, idC, idP, joue_act) VALUES ('" . $idJ . "', '0', '" . $_SESSION['idPartie'] . "', '" . $joueAct . "')");
        }
        
    }
    $_SESSION['score'] = $tabScore;
} else {
    // Mettre à jour les positions des pions
    if (isset($_SESSION['jAct'])) {
        $nouvelle_position = determiner_position($_SESSION['nbCP'], $_SESSION['nbDesR'], $_SESSION['nbDesB'], $_SESSION['nbDesJ']);
        $idJoueur = $_SESSION['jAct'];
        mettre_a_jour_position($idJoueur, $nouvelle_position);
        if(isset($_SESSION['score'][$idJoueur])){
            $_SESSION['score'][$idJoueur] += 5;  
        }else 
            $_SESSION['score'][$idJoueur] = 0;  
    }
}

if (!isset($idPartie)) {
    // Gérer le cas où $idPartie n'est pas défini
}



// Vérifier si le formulaire "choixMain" est soumis et qu'il reste des tentatives
if(isset($_POST['choixMain']) && $_SESSION['tentative'] > 0){
    // Enregistrer les choix de dés dans une variable de session
    $_SESSION['choixDes'] = array(
        'nbDesR' => isset($_POST['nbDesRouges']) ? $_POST['nbDesRouges'] : 0,
        'nbDesB' => isset($_POST['nbDesBleus']) ? $_POST['nbDesBleus'] : 0,
        'nbDesJ' => isset($_POST['nbDesJaunes']) ? $_POST['nbDesJaunes'] : 0
    );
    $_SESSION['faceSelect']['rouge'] = array(); // Réinitialisation du tableau pour les dés rouges
    $_SESSION['faceSelect']['bleu'] = array(); // Réinitialisation du tableau pour les dés bleus
    $_SESSION['faceSelect']['jaune'] = array(); // Réinitialisation du tableau pour les dés jaunes


    $_SESSION['nbDesR'] = $_SESSION['choixDes']['nbDesR'];
    $_SESSION['nbDesB'] = $_SESSION['choixDes']['nbDesB'];
    $_SESSION['nbDesJ'] = $_SESSION['choixDes']['nbDesJ'];


    $nombreDesRouges = $_SESSION['nbDesR'];
    $desRouges = array();
    for ($i = 0; $i < $nombreDesRouges; $i++) {
        // Utiliser rand(1, 6) pour obtenir un nombre aléatoire entre 1 et 6 (inclus)
        $face = rand(1, 6);
    
        // Ajouter la face sélectionnée au tableau $desRouges
        $desRouges[] = $face;
    }
    foreach ($desRouges as $face) {
        $_SESSION['faceSelect']['rouge'][] = $face;
    }

    // Génération de nouvelles faces pour les dés bleus
    $nombreDesBleus = $_SESSION['nbDesB'];
    $desBleus = array();
    for ($i = 0; $i < $nombreDesBleus; $i++) {
        $face = rand(1, 6);
        $desBleus[] = $face;
    }
    foreach ($desBleus as $face) {
        $_SESSION['faceSelect']['bleu'][] = $face;
    }

    // Génération de nouvelles faces pour les dés jaunes
    $nombreDesJaunes = $_SESSION['nbDesJ'];
    $desJaunes = array();
    for ($i = 0; $i < $nombreDesJaunes; $i++) {
        $face = rand(1, 6);
        $desJaunes[] = $face;
    }
    foreach ($desJaunes as $face) {
        $_SESSION['faceSelect']['jaune'][] = $face;
    }

    $_SESSION['tentative'] -= 1 ;
}

if (isset($_POST['RelancerDes']) && $_SESSION['tentative'] > 0) {
    $_SESSION['tentative'] -= 1;

    // Réinitialiser les tableaux des faces sélectionnées pour les dés rouges, bleus et jaunes
    $_SESSION['faceSelect']['rouge'] = array(); // Réinitialisation du tableau pour les dés rouges
    $_SESSION['faceSelect']['bleu'] = array(); // Réinitialisation du tableau pour les dés bleus
    $_SESSION['faceSelect']['jaune'] = array(); // Réinitialisation du tableau pour les dés jaunes

    // Génération de nouvelles faces pour les dés rouges
    $nombreDesRouges = $_SESSION['nbDesR'];
    $desRouges = array();
    for ($i = 0; $i < $nombreDesRouges; $i++) {
        // Utiliser rand(1, 6) pour obtenir un nombre aléatoire entre 1 et 6 (inclus)
        $face = rand(1, 6);
    
        // Ajouter la face sélectionnée au tableau $desRouges
        $desRouges[] = $face;
    }
    foreach ($desRouges as $face) {
        $_SESSION['faceSelect']['rouge'][] = $face;
    }

    // Génération de nouvelles faces pour les dés bleus
    $nombreDesBleus = $_SESSION['nbDesB'];
    $desBleus = array();
    for ($i = 0; $i < $nombreDesBleus; $i++) {
        $face = rand(1, 6);
        $desBleus[] = $face;
    }
    foreach ($desBleus as $face) {
        $_SESSION['faceSelect']['bleu'][] = $face;
    }

    // Génération de nouvelles faces pour les dés jaunes
    $nombreDesJaunes = $_SESSION['nbDesJ'];
    $desJaunes = array();
    for ($i = 0; $i < $nombreDesJaunes; $i++) {
        $face = rand(1, 6);
        $desJaunes[] = $face;
    }
    foreach ($desJaunes as $face) {
        $_SESSION['faceSelect']['jaune'][] = $face;
    }
}


// Lorsque le joueur relance les dés
if(isset($_POST['Avancer'])) { 
    // Générer de nouveaux dés en utilisant les valeurs stockées dans la session
    $_SESSION['faceSelect']['rouge'] = array(); // Réinitialisation du tableau pour les dés rouges
    $_SESSION['faceSelect']['bleu'] = array(); // Réinitialisation du tableau pour les dés bleus
    $_SESSION['faceSelect']['jaune'] = array(); // Réinitialisation du tableau pour les dés jaunes


    // Réinitialiser les tentatives si nécessaire
    if ($_SESSION['tentative'] == 0) {
        if (isset($_SESSION['ordre']) && isset($_SESSION['jAct'])) {
            $_SESSION['jAct'] = prochainJoueur($_SESSION['ordre'], $_SESSION['jAct']);
        } else {
            // Gérer le cas où $_SESSION['ordre'] ou $_SESSION['jAct'] n'est pas défini
        }
        $_SESSION['tentative'] = 3;
    }
}

$donneeMode = executer_une_requete("SELECT mode FROM `Partie` WHERE idP = '" . $_SESSION['idPartie'] . "'");
if (!empty($donneeMode)) {
    // Extraire le mode de la première ligne de résultats
    $mode = $donneeMode[0]['mode'];

    // Mettre à jour $_SESSION['mode'] en fonction de la valeur du mode
    switch ($mode) {
        case 'ALEA':
            $_SESSION['mode'] = "Aléatoire.";
            break;
        case '+JEUNE':
            $_SESSION['mode'] = "Honneur au plus jeune.";
            break;

        case '-EXPE':
            $_SESSION['mode'] = "Honneur au moins expérimenté.";
        default:
            // Mode par défaut si aucune correspondance n'est trouvée
            $_SESSION['mode'] = "Mode inconnu.";
            break;
    }
} else {
    // Aucune donnée trouvée dans la requête
    $_SESSION['mode'] = "Mode non défini.";
}

/*$ordrePassage = ordrePassageJoueurs($_SESSION['mode'], $_SESSION['nbJoueur']);
$_SESSION['ordre'] = $ordrePassage;*/

$donneeEtat = executer_une_requete("SELECT etat FROM `Partie` WHERE idP = '" . $_SESSION['idPartie'] . "'");
if (!empty($donneeEtat)) {
    // Extraire le mode de la première ligne de résultats
    $etat = $donneeEtat[0]['etat'];

    // Mettre à jour $_SESSION['mode'] en fonction de la valeur du mode
    switch ($etat) {
        case 'T':
            $_SESSION['etat'] = "Terminée.";
            break;
        case 'EC':
            $_SESSION['etat'] = "En cours...";
            break;

        case 'AV':
            $_SESSION['etat'] = "A venir..";
        default:
            // Mode par défaut si aucune correspondance n'est trouvée
            $_SESSION['etat'] = "Etat inconnu.";
            break;
    }
} else {
    // Aucune donnée trouvée dans la requête
    $_SESSION['etat'] = "Etat non défini.";
}

$nbJoueurs = count($_SESSION['indice_joueur']);


close_connection_DB();
?>