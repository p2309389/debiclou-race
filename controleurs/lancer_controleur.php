<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php 

$erreur_message = "";
$etape = 0;

//Réception de la première étape du formulaire avec ajout dans la base
if(isset($_POST['form1'])) {
    $_SESSION['idPartie'] = $idPartie;
    $_SESSION['nbCP'] = $_POST["nbCP"];
    $_SESSION['nbCV'] = $_POST["nbCV"];
    $_SESSION['nbCO'] = $_POST["nbCO"];
    $_SESSION['nbCN'] = $_POST["nbCN"];
    $_SESSION['nbJoueur'] = $_POST["nbJoueur"];


    $_SESSION['carte'] = array(
        'idC' => 'valeur_id',
        'niveau' => 'valeur_niveau',
        'points' => 'valeur_points',
        'img' => 'chemin_vers_image'
    );
    
    // Initialisation du deck total avec toutes les cartes disponibles
    $deckTotal = array();

    // Générer les cartes noires
    for ($i = 1; $i <= 9; $i++) {
        $nouvelleCarte = array(
            'idC' => 'N' . str_pad($i, 3, '0', STR_PAD_LEFT),
            'niveau' => 'N',
            'points' => 6,
            'img' => 'N' . str_pad($i, 3, '0', STR_PAD_LEFT) . ".png"
        );
        $deckTotal[] = $nouvelleCarte;
    }

    // Générer les cartes oranges
    for ($i = 1; $i <= 20; $i++) {
        $nouvelleCarte = array(
            'idC' => 'O' . str_pad($i, 3, '0', STR_PAD_LEFT),
            'niveau' => 'O',
            'points' => 4,
            'img' => 'O' . str_pad($i, 3, '0', STR_PAD_LEFT) . ".png"
        );
        $deckTotal[] = $nouvelleCarte;
    }

    // Générer les cartes vertes
    for ($i = 1; $i <= 46; $i++) {
        $nouvelleCarte = array(
            'idC' => 'V' . str_pad($i, 3, '0', STR_PAD_LEFT),
            'niveau' => 'V',
            'points' => 2,
            'img' => 'V' . str_pad($i, 3, '0', STR_PAD_LEFT) . ".png"
        );
        $deckTotal[] = $nouvelleCarte;
    }

    // Tirage aléatoire pour former le deck de la partie en fonction des données du formulaire
    $nbCartesNoires = $_POST["nbCN"];
    $nbCartesOranges = $_POST["nbCO"];
    $nbCartesVertes = $_POST["nbCV"];

    $deckPartie = array();

    // Tirage aléatoire pour les cartes noires
    for ($i = 0; $i < $nbCartesNoires; $i++) {
        $index = array_rand($deckTotal);
        $deckPartie[] = $deckTotal[$index];
        unset($deckTotal[$index]); // Supprimer la carte sélectionnée pour éviter les doublons
    }

    // Tirage aléatoire pour les cartes oranges
    for ($i = 0; $i < $nbCartesOranges; $i++) {
        $index = array_rand($deckTotal);
        $deckPartie[] = $deckTotal[$index];
        unset($deckTotal[$index]); // Supprimer la carte sélectionnée pour éviter les doublons
    }

    // Tirage aléatoire pour les cartes vertes
    for ($i = 0; $i < $nbCartesVertes; $i++) {
        $index = array_rand($deckTotal);
        $deckPartie[] = $deckTotal[$index];
        unset($deckTotal[$index]); // Supprimer la carte sélectionnée pour éviter les doublons
    }

    // Mélanger le deck de la partie
    shuffle($deckPartie);
    // Initialiser $_SESSION['deck']
    $_SESSION['deck'] = array(
        'nbCartes' => count($deckPartie), // Nombre de cartes dans le deck
        'cartes' => $deckPartie // Tableau des cartes
    );    

    // Vérification si nbCP est égal à la somme de nbCV, nbCO et nbCN
    if ($_SESSION['nbCP'] != ($_SESSION['nbCV'] + $_SESSION['nbCO'] + $_SESSION['nbCN'])) {
        $erreur_message = "Erreur : la somme des cartes vertes, oranges et noires ne correspond pas au nombre de cartes sur le plateau.";
    }
    else {
        $etape = 1;
        error_reporting(E_ERROR | E_PARSE);
        $idPartieSQL = executer_une_requete("SELECT MAX(idP) FROM `Partie`");
        $_SESSION['idPartie'] = $idPartieSQL[0]['MAX(idP)'] + 1;
        //$idPartie ++
        $idPartie = $_SESSION['idPartie'];
        $nbJoueurs = $_SESSION['nbJoueur'];
        
        $query = "INSERT INTO Partie (idP, dateP, horaire, duree, etat, nbJoueurs) ";
        $query .= "VALUES ('$idPartie', CURDATE(), CURTIME(), 0, 'EC', '$nbJoueurs')";
        
        $a = executer_une_requete($query);

    }
}

//Réception de la première étape du formulaire avec ajout dans la base
if(isset($_POST['form2'])) {
    error_reporting(E_ERROR | E_PARSE);
    $idPartie = executer_une_requete("SELECT MAX(idP) FROM `Partie`");
    $idPartie = $_SESSION['idPartie']['MAX(idP)'];

    // Récupérer les indices des joueurs entrés par l'utilisateur
    $nbJoueurSQL = executer_une_requete("SELECT nbJoueurs FROM Partie WHERE idP = '" . $_SESSION['idPartie'] . "'");    $_SESSION['nbJoueur'] = $nbJoueurSQL[0]['nbJoueurs'];
    
    $indices_joueurs = array();
    for ($i = 1; $i <= $_SESSION['nbJoueur'] ; $i++) {
        $indice_joueur[$i] = $_POST["joueur_$i"];
    }
    $_SESSION['indice_joueur'] = $indice_joueur;

    $pseudoJoueur = array();
    foreach($indice_joueur as $identifiant){
        $donneesPseudo = executer_une_requete("SELECT pseudo FROM `JOUEUR` WHERE idJ = '$identifiant'");
        // Vérification si des données sont retournées
        if(!empty($donneesPseudo)) {
            $pseudoJoueur[$identifiant] = $donneesPseudo[0]['pseudo'];
        } else {
            // Gérer le cas où aucun pseudo n'est trouvé pour un identifiant spécifique
            $pseudoJoueur[$identifiant] = "Pseudo non trouvé";
        }
    }
    $_SESSION['pseudo'] = $pseudoJoueur;

    $_SESSION['strategie'] = $_POST['strategie'];

    $_SESSION['idJ'] = array_unique($indice_joueur);

   
    // Définir un tableau des couleurs disponibles
    $couleurs_disponibles = array('rouge', 'vert', 'bleu', 'jaune', 'orange');

    // Tableau pour stocker les associations joueur-couleur de pion
    $pions_joueurs = array();

    // Associer chaque joueur à une couleur de pion
    foreach ($indice_joueur as $identifiant) {
        // Calculer l'index de couleur en utilisant le modulo pour boucler sur les couleurs disponibles
        $index_couleur = count($pions_joueurs) % count($couleurs_disponibles);
        
        // Associer le joueur à une couleur de pion
        $pions_joueurs[$identifiant] = $couleurs_disponibles[$index_couleur];
    }

    // Stocker les associations joueur-couleur en session
    $_SESSION['pion'] = $pions_joueurs;


    if(count($indice_joueur) === count($_SESSION['idJ']) && ($indice_joueur[1] !== '0') && ($indice_joueur[2] !== '0') && ($indice_joueur[3] !== '0') && ($indice_joueur[4] !== '0') && ($indice_joueur[5] !== '0') && ($_SESSION['strategie'] !== 'Nope')){
        // Obtenir l'ordre de passage des joueurs
        $ordre = ordrePassageJoueurs($_SESSION['strategie'], $indice_joueur);
        $_SESSION['ordre'] = $ordre;
        
        // Définir le joueur actuel comme le premier joueur dans l'ordre
        $_SESSION['jAct'] = $ordre[0];
        $_SESSION['tentative'] = 3;

        $_SESSION['nbDesR'] = 0;
        $_SESSION['nbDesB'] = 0;
        $_SESSION['nbDesJ'] = 0;
        $_SESSION['nbDesRmax'] = 4;
        $_SESSION['nbDesBmax'] = 4;
        $_SESSION['nbDesJmax'] = 4;
        $_SESSION['faceSelect'] = array(
            'rouge' => array(),
            'bleu' => array(),
            'jaune' => array()
        );

        $_SESSION['position'] = initialiser_positions($indice_joueur);        
        
        for ($i = 1; $i <= $_SESSION['nbJoueur']; $i++) {
            $a = executer_une_requete("INSERT INTO JOUE (idJ, idP, ordre, score, couleur_pion) VALUES ('" . $indice_joueur[$i] . "', '" . $_SESSION['idPartie'] . "', '" . $ordre[$i] . "' , '0', '" . $pion[$i] . "')");  
        }
        // Récupérer la stratégie choisie par l'utilisateur
    
        $a = executer_une_requete("UPDATE Partie SET mode = '" . $_SESSION['strategie'] . "' WHERE idP = '" . $_SESSION['idPartie'] . "'");        $etape = 2;
    }
    else {
        $etape = 1;
        $erreur_message = "Erreur : Veuillez saisir tous les champs du formulaire.";

    }
}
?>