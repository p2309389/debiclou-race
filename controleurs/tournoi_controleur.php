<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php 

$erreur_message = "";
$etape = 0;
global $tabParam;

//Réception de la première étape du formulaire avec ajout dans la base
if(isset($_POST['form1'])) {
    $_SESSION['nom'] = $_POST['nom'];
    $_SESSION['dateDebut'] = $_POST['dateDebut'];
    $_SESSION['dateFin'] = $_POST['dateFin'];
    $_SESSION['nbParticipants'] = $_POST['nbParticipants'];


    $nom = $_SESSION['nom'];
    $dateDebut = $_SESSION['dateDebut'];
    $dateFin = $_SESSION['dateFin'];

    $etape = 1;
    error_reporting(E_ERROR | E_PARSE);
    $_SESSION['idTournoi'] = executer_une_requete("SELECT MAX(idT) FROM `TOURNOI`");
    $idTournoi = $_SESSION['idTournoi'][0]['MAX(idT)'] + 1;
    $a = executer_une_requete("INSERT INTO TOURNOI (idT, nom, dateDeb, dateFin) VALUES ('$idTournoi', \"$nom\", '$dateDebut', '$dateFin') ");
}

//Réception de la 2e étape du formulaire avec ajout dans la base
if(isset($_POST['form2'])) {
    error_reporting(E_ERROR | E_PARSE);
    $indice_joueur = array();
    for ($i = 1; $i <= $_SESSION['nbParticipants']; $i++) {
        $indice_joueur[$i] = $_POST["joueur_$i"];        
    }

    switch($_SESSION['nbParticipants']) {
        case 2:
            $niveau = 'Finale';
            break;
        case 4:
            $niveau = 'Demi-finale';
            break;
        case 8:
            $niveau = 'Quart de finale';
            break;
        case 16:
            $niveau = 'Huitième de finale';
            break;
        default:
            $niveau = 'Erreur dans le switch';
            break;
    }
    

    $idTournoi = $_SESSION['idTournoi'][0]['MAX(idT)'] + 1;
    $indices_uniques = array_unique($indice_joueur);

    if(count($indice_joueur) === count($indices_uniques) && ($indice_joueur[1] !== '0') && ($indice_joueur[2] !== '0') && ($indice_joueur[3] !== '0') && ($indice_joueur[4] !== '0') && ($indice_joueur[5] !== '0')){
        for ($i = 1; $i <= $_SESSION['nbParticipants']; $i++) {
            $a = executer_une_requete("INSERT INTO PARTICIPE (idJ, Niveau, a_joue, idT) VALUES ($indice_joueur[$i], '$niveau', 0, $idTournoi)");
        }
        
        $etape = 0;
    }
    else {
        $etape = 1;
        $erreur_message = "Erreur : Veuillez saisir tous les champs du formulaire.";

    }

}

//Fonction pour récupérer et afficher les tournois en cours et leurs vainqueurs dans la base de données
function tournoisEnCours() {
    $phaseTournoi = executer_une_requete("SELECT T.nom, T.dateFin, T.dateDeb FROM TOURNOI T WHERE T.dateDeb < CURDATE() AND T.dateFin > CURDATE() ORDER BY T.dateDeb");
    
    echo("<h2>Tournois en cours :</h2>");

    echo "<table border='1'>";
    echo "<tr><th>Tournoi</th><th>Date de début</th><th>Date de fin</th></tr>";
    
    foreach ($phaseTournoi as $row) {
        echo "<tr><td>" . $row['nom'] . "</td><td>" . $row['dateDeb'] . "</td><td>" . $row['dateFin'] . "</td></tr>";
    }
    
    echo "</table>";   
}

//Fonction pour récupérer et afficher les tournois passés dans la base de données
function tournoisPasses() {
    $phaseTournoi = executer_une_requete("SELECT T.nom AS tournoi, T.dateFin, T.dateDeb, J.prénom, J.nom FROM TOURNOI T JOIN PARTICIPE P ON P.idT = T.idT JOIN JOUEUR J ON J.idJ = P.idJ WHERE T.dateFin < CURDATE() AND P.Niveau = 'Finale' AND P.est_qualifie = 1 ORDER BY T.dateDeb");
    
    echo("<h2>Tournois passés :</h2>");

    echo "<table border='1'>";
    echo "<tr><th>Tournoi</th><th>Date de début</th><th>Date de fin</th><th>Vainqueur</th></tr>";
    
    foreach ($phaseTournoi as $row) {
        echo "<tr><td>" . $row['tournoi'] . "</td><td>" . $row['dateDeb'] . "</td><td>" . $row['dateFin'] . "</td><td>" . $row['prénom'] . ", " . $row['nom'] . "</td></tr>";
    }
    
    echo "</table>";   
}

//Fonction pour récupérer et afficher les tournois à venir dans la base de données
function tournoisAVenir() {
    $phaseTournoi = executer_une_requete("SELECT T.nom, T.dateFin, T.dateDeb FROM TOURNOI T WHERE T.dateDeb > CURDATE() ORDER BY T.dateDeb");
    
    echo("<h2>Tournois à venir :</h2>");

    echo "<table border='1'>";
    echo "<tr><th>Tournoi</th><th>Date de début</th><th>Date de fin</th></tr>";
    
    foreach ($phaseTournoi as $row) {
        echo "<tr><td>" . $row['nom'] . "</td><td>" . $row['dateDeb'] . "</td><td>" . $row['dateFin'] . "</td></tr>";
    }
    
    echo "</table>";   
}
?>