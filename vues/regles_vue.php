<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<div class="bloc_commandes">
	<body>
		<!--Affichage des règles-->
		<h1>Règles du jeu</h1>

		<p>Le principe du jeu est le suivant : chaque joueur est un cycliste qui doit arriver le premier au bout du parcours qui est composé de 12 cartes. Pour avancer sur une case, il faut que le joueur réalise la combinaison de
		dés demandée sur la carte. Pour cela, chaque joueur dispose de 6 dés de couleur bleue, 6 dés de couleur jaune et 6 dés de couleur rouge. À chaque tour, le joueur choisit 6 dés parmi les 18 dés de couleurs pour essayer de valider une ou plusieurs cases en au plus 3 lancers. Différentes stratégies peuvent être mises en place en fonction du nombre de cartes que le joueur souhaite valider en un tour.</p>

		<p>Dans un premier temps, nous considérons les joueurs. Les joueurs sont identifiés par un entier numérique et on stocke le nom, le prénom, la date de naissance, un pseudo et une adresse mail.
		Un joueur joue des parties. Une partie est identifiée par une valeur numérique et on stocke la date et l’heure du début de partie ainsi que la durée une fois que la partie est terminée. Une partie a donc un état qui peut être ’A venir’ (partie paramétrée et créée mais non encore commencée), ’En cours’ (partie en cours de jeu ou interrompue) et ’Terminée’ (partie où tous les joueurs ont fini le parcours).</p>

		<p>Pour chaque partie, on dispose d’un plateau composé de plusieurs cartes. Chaque plateau a un nombre de cartes fixé. Par défaut, un plateau est composé de 12 cartes.</p>

		<p>Pour chaque carte, on dispose d’une image pour représenter le visuel de la carte. Une carte dispose d’un niveau (vert, orange ou noir) en fonction du niveau de difficulté de celle-ci. Chaque carte dispose également
		de points, là encore définis en fonction de la difficulté de la carte. Une carte est associée à une ou plusieurs contraintes de base. Ces contraintes ont un nom et on en considère quatre :

			- une contrainte nommée "face de dé" représentée par le couple (coul,val), où coul ∈ {’rouge’,’jaune’,’bleu’} et val ∈ {1, 2, 3, 4, 5, 6} : Pour valider cette contrainte, le joueur doit obtenir au moins un dé de couleur coul avec la valeur val.

			- une contrainte nommée "seuil de dés" représentée par le triplet (coul, seuil,sens), où coul ∈ {’rouge’,’jaune’,’bleu’}, seuil ∈ {2, . . ., 36} et sens ∈ {<, >} : Pour valider cette contrainte, le joueur doit obtenir un score cumulé de valeurs de dés de couleur coul qui soit strictement supérieur ou inférieur (selon la valeur de sens) à la valeur seuil.

			- une contrainte nommée "les mêmes au choix" représentée par le couple (coul, nb), où coul ∈ {’rouge’,’jaune’,’bleu’} et nb ∈ {2, 3, 4} : Pour valider cette contrainte, le joueur doit obtenir au moins nb dés de même valeur et de couleur coul.

			- une contrainte nommée "suite au choix" représentée par le singleton (coul, nb), où coul ∈ {’rouge’,’jaune’,’bleu’} et nb ∈ {2, 3, 4} : Pour valider cette contrainte, le joueur doit obtenir au moins nb dés de valeurs successives et de couleur coul.</p>
			
		<p>À partir de ces contraintes, il est possible d’associer à chaque carte une ou plusieurs contraintes. Ainsi, la Figure 1 représente trois cartes associées à une ou plusieurs contraintes "face de dés".
		La carte C005.png est associée à une contrainte "face de dé" (’bleu’,5). La carte C020.png est associée aux contraintes "face de dé" (’bleu’,2) et (’rouge’,4). La carte C039.png est associée aux contraintes "face de dé"
		(’jaune’,3), (’rouge’,3) (’bleu’,3). À noter que C039.png n’est pas associée à une contrainte "les mêmes au choix" car les couleurs imposées par dé sont différentes.</p>

		<img class="image" src="img/FIGURE 1.png" alt="FIGURE 1"><br> – Exemple de 3 cartes associées à une ou plusieurs contraintes "face de dés".

		<img class="image" src="img/FIGURE 2.png" alt="FIGURE2"><br> – Exemple de cartes associées à des contraintes "face de dé", "seuil de dés" et "les mêmes au choix".

		<p>La carte C053.png est associée à une contrainte "seuil de dés" (’jaune’,14, ’>’). La carte C072.png est associée aux contraintes "seuil de dés" (’rouge’,4, ’<’) et "face de dé" (’jaune’,2). La carte C056.png est associée à la contrainte "les mêmes au choix" (’bleu’,3).


		<p>Pour finir, la Figure 3 représente trois cartes associées à des contraintes "face de dé", "les mêmes au choix" et "suite au choix".

		<img class="image" src="img/FIGURE 3.png" alt="FIGURE3"><br> – Exemple de 3 cartes associées à une ou plusieurs contraintes "face de dés"

		<p>La carte C070.png est associée aux contraintes "les mêmes au choix" (’jaune’, 2) et "face de dé" (’jaune’, 2).
			La carte C068.png est associée à la contrainte "série au choix" (’rouge’, 3). La carte C073.png est associée aux contraintes "les mêmes au choix" (’bleu’,2) et "série au choix" (’rouge’, 2).
			À noter que sur les 3 figures, le niveau des cartes est représenté par la couleur de fond de la pastille des 3 points. Les cartes C005.png et C020.png sont des cartes vertes valant 1 point. Les cartes C053.png et C072.png sont des cartes vertes valant 2 points. Les cartes C039.png, C056.png, C070.png et C068.png sont des cartes oranges valant 4 points. Enfin la carte C073.png est une carte noire valant 5 points.
			Au début d’une partie, une couleur de pion est attribuée aléatoirement à chaque joueur. Chaque pion est placé sur la carte ’départ’.
			Un ordre de passage est choisi. Trois modes sont possibles :

			— Un mode dit honneur au plus jeune : l’ordre de passage se fait du plus jeune au plus ancien en fonction de la date de naissance des joueurs. Pour les joueurs nés le même jour, l’ordre de passage se fera en fonction du nom selon l’ordre lexicographique.
			
			— Un mode dit honneur au moins expérimenté : l’ordre de passage se fait du joueurs ayant fait le moins de parties à celui qui en a fait le plus. Pour les joueurs ayant fait le même nombre de parties, l’ordre de passage se fera en fonction du nom selon l’ordre lexicographique.
			
			— Un mode dit aléatoire : l’ordre de passage se fait de manière aléatoire.
			
			<p>Une partie se déroule en plusieurs tours (autant que nécessaires pour que tous les joueurs franchissent la carte ’arrivée’).
			À chaque tour, chaque joueur choisit une main constituée de 6 dés dont un certain nombre de dés rouges, de dés jaunes et de dès bleus. À partir de cette main, le joueur a droit à au plus 3 lancers. Après chaque lancer, le
			joueur sélectionne le ou les dés qui permettent de valider une ou plusieurs contraintes de la prochaine carte sur laquelle il veut avancer. Si toutes les contraintes de la carte sont validées, le pion du joueur peut alors avancer
			sur cette carte.
			En termes de stratégie pour avancer plus vite, le joueur peut chercher à valider plusieurs cartes lors d’un même tour. Toutefois, s’il se trouve sur la carte n, qu’il arrive à valider entièrement les contraintes de la carte n+2 mais que la carte n+1 n’est pas entièrement validée au bout des 3 lancers, le pion du joueur n’avancera pas.
			L’ordre d’arrivée des joueurs permet d’établir le classement final.
			La gestion des scores se fait de la manière suivante :

			- Pendant la partie, chaque joueur cumule les points des cartes qu’il a validées, points qui sont pondérées de la manière suivante, sachant qu’une tentative de validation correspond à un tour (et non un lancer) :
			
			— le joueur cumule 3 fois le nombre de points de la carte, s’il la valide à la première tentative ;
			
			— le joueur cumule 2 fois le nombre de points de la carte, s’il la valide à la seconde tentative ;
			
			— le joueur cumule le nombre de points de la carte, s’il la valide à la troisième tentative ;
			
			— le joueur ne cumule aucun point au-delà de la troisième tentative.
			
			
			- Pendant un tour, le nombre de points cumulés durant le tour est multiplié par le nombre de cartes validées durant ledit tour.
				
			- À l’arrivée d’un joueur, son score correspond au nombre de points cumulés durant la partie multiplié par la pondération δ, où δ correspond au nombre total de joueurs de la partie auquel on retranche le rang final
			dudit joueur augmenté de un. Ainsi, pour une partie avec 5 joueurs, le joueur de rang 1 verra son nombre de points cumulés multiplié par (5-1+1=) 5 et le dernier verra son nombre de points cumulés multiplié par (5-5+1) = 1.
		</p>
		<h2>Éléments très importants :</h2> 

		<p>L’application doit permettre à un joueur de se déconnecter (volontairement ou non) et de pouvoir reprendre la partie là où il en était. Ainsi, la position des pions sur le plateau, la valeur des dés lancés durant un tour, le nombre de points cumulés, le nombre de tentatives de validation d’une carte par un joueur doivent être mémorisés en base.
			En marge des parties, on souhaite gérer également l’organisation de tournois et les classements de joueurs.</p>

		<p>Concernant les tournois, un tournoi est identifié par un entier numérique et il a un nom, une date de début et une date de fin. Un tournoi se décompose en phase. Une phase est identifiée relativement au tournoi par rapport à un niveau ’Poule de sélection’, ’Seizième de finale’, ’Huitième de finale’, ’Quart de finale’, ’Demi-finale’ et ’Finale’. Un joueur participe à une phase de tournoi et on stocke l’information si le joueur a joué la phase et s’il s’est qualifié pour la phase suivante ou pas. À noter que si le joueur est qualifié pour la phase de niveau ’Finale’, cela signifie qu’il a gagné le tournoi. Les différentes éditions des tournois sont gérées par le fait que les 4tournois peuvent être en lien les uns avec les autres. Ainsi, le tournoi nommé ’DéBiclou Race France 2024’ est en lien avec le tournoi nommé ’DéBiclou Race France 2023’. Le type de lien entre les deux tournois est un lien d’édition.</p>

		</p>Les classements se font en fonction des points cumulés et du nombre de parties jouées. Un classement est identifié par un entier et on stocke la portée du classement (’internationale’, ’nationale’, ’régionale’. . .). Il existe alors différents classements. Les joueurs peuvent être classés de manière individuelle ou en équipe. En effet, un joueur peut être membre d’une équipe. Les résultats des membres d’une équipe sont agrégés, ce qui permet de classer les équipes. Au même titre que les tournois, les classements peuvent être en lien. Ainsi, le Classement international ’World DébiclouRace Ranking’ est en lien avec le classement national ’DéBiclou Race France’. Les types de lien peuvent être hiérarchiques.</p>
	</body>
</div>