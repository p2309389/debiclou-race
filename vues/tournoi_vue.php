<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<div class="panneau">

  <div class="panneau_details"> 
    <!--Ajout des infos tournois avant le formulaire si il n'a pas été commencé-->
    <?php
        if($etape == 0){
        tournoisEnCours();
        tournoisAVenir();
        tournoisPasses();
    ?>
    <!--Formulaire d'ajout d'un nouveau tournoi partie 1-->
	<h2>Ajouter un tournoi :</h2>
    <form class="bloc_commandes" method="post" action="#">	
        <div class="input-container">
            <label for="nom">Nom du tournoi :</label>		
            <input type="text" name="nom" id="nom" placeholder="Entrez le nom du tournoi">
        </div>
        <br><br>
        <div class="input-container">
            <label for="dateDebut">Date de début :</label>		
            <input type="date" name="dateDebut" id="dateDebut" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="YYYY-MM-DD">
        </div>

        <br><br>
        <div class="input-container">
            <label for="dateFin">Date de fin :</label>		
            <input type="date" name="dateFin" id="dateFin" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="YYYY-MM-DD">
        </div>
        <br><br>
        <div class="select-container">
            <label for="nombreParticipants">Nombre de particpants :</label>		
            <select name="nbParticipants" id="nbP">
                <?php
                for ($i = 2; $i <= 16; $i++) {
                    if($i == 2 || $i == 4 || $i == 8 || $i == 16){
                        echo "<option value=\"$i\">$i</option>";
                    }
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <input type="submit" name="form1" value="Suivant"/>
        </div>
        <p><?php echo $erreur_message; ?></p>
    </form>
    <?php

    }else if ($etape == 1){
    ?>
    <!--Si partie 1 validée, on passe à la deuxième étape d'ajout d'un tournoi : inscription des joueurs-->
    <form class="bloc_commandes" method="post" action="#">	
        <h3>Inscriptions des joueurs au tournoi :</h3>


        <?php 

        global $tabParam;
        for ($i = 1; $i <= $_SESSION['nbParticipants']; $i++) {?>
        <div class="select-container">

            <label for=<?php echo "joueur_$i"?>> <?php echo "Joueur $i"?> </label>		
            <select name=<?php echo "joueur_$i"?> id=<?php echo "joueur_$i"?>>
                <?php
                    open_connection_DB();
                    $donnees = executer_une_requete("SELECT MAX(idJ) FROM `JOUEUR`");
                    $max_idJ = $donnees[0]['MAX(idJ)'];
                    


                    
                    // Parcours de la bdd pour récupérer les noms, prenoms des joueur. On répète l'opération selon le nombre de joueurs à inscrire
                    for ($j = 0; $j <= $max_idJ; $j++) {
                        if($j == 0){
                            echo "<option value='$j'>Séléctionner un joueur</option>";
                        }
                        else{
                            $donneesNom = executer_une_requete("SELECT nom FROM `JOUEUR` WHERE idJ = '$j'");
                            $nomJoueur = $donneesNom[0]['nom'];
    
                            $donneesPrenom = executer_une_requete("SELECT prénom FROM `JOUEUR` WHERE idJ = '$j'");
                            $prenomJoueur = $donneesPrenom[0]['prénom'];
    
                            $donneesPseudo = executer_une_requete("SELECT pseudo FROM `JOUEUR` WHERE idJ = '$j'");
                            $pseudoJoueur = $donneesPseudo[0]['pseudo'];
    
                            if (!empty($nomJoueur)) {
                                echo "<option value='$j'>$prenomJoueur $nomJoueur, $pseudoJoueur</option>";
                            }
                        }

                    }
                ?>
            </select>
        </div>
        <br><br>
        <?php } ?>

        <div class="select-container">
            <input type="submit" name="form2" value="Suivant"/>
        </div>
        <p><?php echo $erreur_message; ?></p>
    </form>
    <!--Ajout des infos tournois après le formulaire si il a été commencé-->

    <?php
        tournoisEnCours();
        tournoisAVenir();
        tournoisPasses();
        } else{ // Redirection vers une autre page
            header("Location: index.php?page=jouerPartie");
        } 
    ?>

</div>