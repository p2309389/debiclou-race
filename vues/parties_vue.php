<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<div class="panneau">


  <div class="panneau_details"> 
	<!-- Affichage des parties en bases avec + d'info sur clic-->
	<h2>Détails des parties terminées et en cours</h2>

	<div>
		<?php
			if( is_array($resultats) ){ 
		?>
			<table class="table_resultat">
				<thead>
					<tr>
					<?php
						//var_dump($resultats);
						foreach($resultats['schema'] as $att) {  // pour parcourir les attributs
							if ( isset($_GET['id'])){
								echo '<th>';
								echo $att['nom'];
								echo '</th>';
							}
							else if ($att['nom'] == 'idP'){
								echo '<th>';
								echo $att['nom'];
								echo '</th>';
							}
							
						}
					?>	
					</tr>	
				</thead>
				<tbody>

				<?php
					
					foreach( $resultats['instances'] as $row ) {  // pour parcourir les n-uplets
				
					echo '<tr>';
					foreach($row as $key => $valeur) { // pour parcourir chaque valeur de n-uplets
						if ($key == 'idP')
						echo '<td><a href="index.php?page=parties&id='. $valeur .'">' . "Partie " . $valeur . '</a></td>';
					}
					echo '</tr>' ;
					
					if (isset($_GET['id']) && $_GET['id'] == $row['idP']) {
						echo '<tr>';
						foreach($row as $key => $valeur) { // pour parcourir chaque valeur de n-uplets
							echo '<td>' . $key . ": " . $valeur . '</td>';
						}	
						echo '</tr>';
					}
				}
			?>
			</tbody>
		</table>
		
		<?php }else{ ?>

			<p class="notification"><?= $message_details . 'TOOT' ?></p>	

		<?php }

	 ?>
	</div>


</div>


</div>